import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/service.dart';
import 'package:untitled/theme/colors/light_colors.dart';

import 'minimal_screens/navbar.dart';

class location extends StatefulWidget {
  static CircleAvatar calendarIcon() {
    return CircleAvatar(
      radius: 25.0,
      backgroundColor: LightColors.kGreen,
      child: Icon(
        Icons.open_in_new_rounded,
        size: 20.0,
        color: Colors.white,
      ),
    );
  }

  @override
  State<location> createState() => _locationState();
}

class _locationState extends State<location> {
  final Stream<QuerySnapshot> _locationStream = FirebaseFirestore.instance
      .collection('/companies/mWazXBjLxxvHnvTBXZq9/locations')
      .snapshots();
  CollectionReference location = FirebaseFirestore.instance
      .collection('/companies/mWazXBjLxxvHnvTBXZq9/locations');
  Future<void> delLocation(userId) {
    return location
        .doc(userId)
        .delete()
        .then((value) => print('Location Delete'))
        .catchError((error) => print('Failed to dalete Location: $error'));
  }

  @override
  Widget build(BuildContext context) {
    print('employee');
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Employee",
          style: TextStyle(fontFamily: 'Kanit'),
        ),
        actions: <Widget>[],
      ),
      body: SafeArea(
        child: StreamBuilder(
          stream: _locationStream,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Something went wrong');
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text('Loading..');
            }
            return ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;

                return ListTile(
                  title: Text(data['location']),
                  subtitle: Text(data['opening_time']),
                  trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () async {
                      await delLocation(document.id);
                    },
                  ),
                );
              }).toList(),
            );
          },
        ),
      ),
    );
    // @override
    // Widget build(BuildContext context) {
    //   return StreamBuilder(
    //     stream: _locationStream,
    //     builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    //       if (snapshot.hasError) {
    //         return Text('Something went wrong');
    //       }
    //       if (snapshot.connectionState == ConnectionState.waiting) {
    //         return Text('Loading..');
    //       }
    // return ListView(
    //   children: snapshot.data!.docs.map((DocumentSnapshot document) {
    //     Map<String, dynamic> data =
    //         document.data()! as Map<String, dynamic>;

    //     return ListTile(
    //       title: Text(data['location']),
    //       subtitle: Text(data['opening_time']),
    //       trailing: IconButton(
    //         icon: Icon(Icons.delete),
    //         onPressed: () async {
    //           await delLocation(document.id);
    //         },
    //       ),
    //     );
    //   }).toList(),
    // );
    //     },
    //   );
  }
}
