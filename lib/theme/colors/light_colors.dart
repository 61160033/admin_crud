import 'package:flutter/material.dart';

class LightColors  {
  static const Color kLightYellow = Color(0xFFFFFFFF);
  static const Color kLightYellow2 = Color(0xFFFFFFFF);
  static const Color kDarkYellow = Color(0xFF8FECFF);
  static const Color kPalePink = Color(0xFFFED4D6);

  static const Color kRed = Color(0xFFE46472);
  static const Color kLavender = Color(0xFFD5E4FE);
  static const Color kBlue = Color(0xFF6488E4);
  static const Color kLightGreen = Color(0xFFD9E6DC);
  static const Color kGreen = Color(0xFF41BFC6);

  static const Color kDarkBlue = Color(0xFF0D253F);
  static const Color kTest = Color(0xFF232323);
  static const Color kGreenSmooth = Color(0xFF3AA55C);
  static const Color kRedSmooth = Color(0xFFED4245);
}