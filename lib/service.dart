var mockLocation = ['informatic1', 'informatic2'];

Future<List> getLocation() {
  return Future.delayed(Duration(seconds: 1), () => mockLocation);
}
