import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/theme/colors/light_colors.dart';

import 'minimal_screens/navbar.dart';

class employee extends StatefulWidget {
  static CircleAvatar calendarIcon() {
    return CircleAvatar(
      radius: 25.0,
      backgroundColor: LightColors.kGreen,
      child: Icon(
        Icons.open_in_new_rounded,
        size: 20.0,
        color: Colors.white,
      ),
    );
  }

  @override
  State<employee> createState() => _employeeState();
}

class _employeeState extends State<employee> {
  final Stream<QuerySnapshot> _userStream = FirebaseFirestore.instance
      .collection('/companies/mWazXBjLxxvHnvTBXZq9/certifiers')
      .snapshots();
  CollectionReference users = FirebaseFirestore.instance
      .collection('/companies/mWazXBjLxxvHnvTBXZq9/certifiers');
  Future<void> delUser(userId) {
    return users
        .doc(userId)
        .delete()
        .then((value) => print('User Delete'))
        .catchError((error) => print('Failed to dalete user: $error'));
  }

  @override
  Widget build(BuildContext context) {
    print('employee');
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Employee",
          style: TextStyle(fontFamily: 'Kanit'),
        ),
        actions: <Widget>[],
      ),
      body: SafeArea(
        child: StreamBuilder(
          stream: _userStream,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Something went wrong');
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text('Loading..');
            }
            return ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                DocumentReference certifier = document.get('certifier');
                return FutureBuilder(
                  future: Future.wait([certifier.get()]),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return ListTile(
                      title: Text(snapshot.data[0]['name']),
                      subtitle: Text(data['role']),
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () async {
                          await delUser(document.id);
                        },
                      ),
                    );
                  },
                );
              }).toList(),
            );
          },
        ),
      ),
    );
  }
}
