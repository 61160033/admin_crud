import 'package:flutter/material.dart';
import 'package:untitled/minimal_screens/navbar.dart';
import 'package:untitled/theme/colors/light_colors.dart';

class HomePage extends StatefulWidget {
  static CircleAvatar calendarIcon() {
    return CircleAvatar(
      radius: 25.0,
      backgroundColor: LightColors.kGreen,
      child: Icon(
        Icons.open_in_new_rounded,
        size: 20.0,
        color: Colors.white,
      ),
    );
  }

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Text subheading(String title) {
    return Text(
      title,
      style: TextStyle(
        color: LightColors.kDarkBlue,
        fontSize: 20.0,
        fontWeight: FontWeight.w700,
        letterSpacing: 1.2,
        fontFamily: 'Kanit',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('home');
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Home",
          style: TextStyle(fontFamily: 'Kanit'),
        ),
        actions: <Widget>[],
      ),
      body: SafeArea(
        child: Column(),
      ),
    );
  }
}
