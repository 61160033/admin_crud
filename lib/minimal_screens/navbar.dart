import 'package:flutter/material.dart';
import 'package:untitled/employee.dart';
import 'package:untitled/location.dart';
import 'package:untitled/main.dart';

import '../homepage.dart';

class NavBar extends StatelessWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        UserAccountsDrawerHeader(
          accountName: Text('Name'),
          accountEmail: Text('Lastname'),
          currentAccountPicture: CircleAvatar(
            child: ClipOval(
              child: Image.network(
                ('https://picsum.photos/250?image=9'),
                width: 90,
                height: 90,
                fit: BoxFit.cover,
              ),
            ),
          ),
          decoration: BoxDecoration(
            color: Colors.blue,
            image: DecorationImage(
              image: NetworkImage(
                'https://data.1freewallpapers.com/download/calm-body-of-water-surrounded-with-trees-and-mountains-during-daytime-4k-nature.jpg',
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.home),
          title: Text(
            'Home',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => HomePage())),
        ),
        ListTile(
          leading: Icon(Icons.book),
          title: Text(
            'Employee',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => employee())),
        ),
        ListTile(
          leading: Icon(Icons.book),
          title: Text(
            'Location',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => location())),
        ),
        ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text(
            'Sign out',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(
                'Are you sure?',
                style: TextStyle(
                  fontFamily: 'Kanit',
                ),
              ),
              content: Text(
                'Do you want to sign out from the app?',
                style: TextStyle(
                  fontFamily: 'Kanit',
                ),
              ),
              actions: [
                GestureDetector(
                  child: Text(
                    'SIGN OUT',
                    style: TextStyle(
                      fontFamily: 'Kanit',
                    ),
                  ),
                ),
                GestureDetector(
                  child: Text(
                    'CANCLE',
                    style: TextStyle(
                      fontFamily: 'Kanit',
                    ),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
